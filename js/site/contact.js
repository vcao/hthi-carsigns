var contact = {

    init: function($) {
        $("input[name=phone]").mask("(999) 999-9999");
    }
};

jQuery(document).ready(function($) {
    contact.init($);
});