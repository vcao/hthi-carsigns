var upload_design = {

    init: function($) {

        // Update file limit notice
        upload_design.updateFileLimitNotice($);

        // Update File name when selected
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
              numFiles = input.get(0).files ? input.get(0).files.length : 1,
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        $(document).on('fileselect', '.btn-file :file', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find('.file-input-label').first(),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
            if( input.length ) {
                input.val(log);
            }
            
        });

        // Remove File Field
        $(document).on('click', '.files .file .remove', function() {
            var input = $(this);

            input.parents('.file-box').remove();

            upload_design.updateFileLimitNotice($);

        });

        // Remove Uploaded File
        $(document).on('click', '.uploaded-files .file .remove', function() {
            var input = $(this).parent().parent().find('.form-control');
            console.log(input.data('path'));

            $.ajax({
                url: wc_add_to_cart_params.ajax_url,
                type: 'POST',
                data: {
                    'action':'remove_uploaded_design',
                    'file': input.data('path'),
                    'cart_key': input.data('cartkey')
                },
            })
            .success(function(data){
                if (data.result == true) {
                    input.parents('.row').first().remove();
                }

                upload_design.updateFileLimitNotice($);
            });
            

            //input.parents('.row').remove();

            upload_design.updateFileLimitNotice($);

        });

        // Add File Field
        $(document).on('click', '#add-file', function() {
            var fileCount = $('.files .file-box').length;
            var uploadedCount = $('.uploaded-files .file-box').length;

            var totalCount = fileCount + uploadedCount;

            //var fileIndex = $('.files .file-box .file-input').length;

            if (totalCount < 6) {
                var source   = $("#file-template").html();
                var template = Handlebars.compile(source);
                var context = {};
                var html    = template(context);

                $('.files').append(html);

                

                upload_design.addFileRules($);
            };

            upload_design.updateFileLimitNotice($);
            
        });

        $.validator.addMethod("uploadfile", function (val, element) {

            var size = element.files[0].size;

            if (size > 10485760)// checks the file more than 1 MB
            {
                return false;
            } else {
                return true;
            }

        }, "File type error");

        $("#upload-files-form").validate({
            errorPlacement: function(error, element) {
                error.appendTo( element.parents(".file-box") );
            }
        });

        // Add file rules
        upload_design.addFileRules($);

        $("#upload-btn").click(function(event){
            var form = $(this);
            setTimeout(function(){
                var validator = $( this ).validate();
                var result = validator.form();
                form.submit();
            }, 1000);
        });

    },

    addFileRules: function($) {
        $('.files .file-box .file-input').each(function (index, val) { 
            $(this).attr('name', 'design_files['+index+']');
            $(val).rules("add", {
                required: true,
                uploadfile: true,
                messages: {
                    required: "Please select a file to upload for your design.",
                    uploadfile: "Please select a file with a size smaller than 10mb."
                }
            });
        });
    },

    updateFileLimitNotice: function($) {
        var fileCount = $('.files .file-box').length;
        var uploadedCount = $('.uploaded-files .file-box').length;

        var totalCount = fileCount + uploadedCount;

        if (totalCount < 6) {
            $('#file-limit-notice').addClass('woocommerce-info').html("<strong>You have added "+totalCount+" out of 6 allowed files.</strong>");
        };
    }
}

jQuery(document).ready(function($) {
    upload_design.init($);
});