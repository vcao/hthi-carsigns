/* global TweenMax, Quad */
/**
*
* X Studios JS
* @author JoeShon Monroe
* @website xstudios.agency
*
**/

var xstudios = {

    init: function($) {

        xstudios.slider($);

    },

    resize: function() {

    },

    slider: function($) {
        $( '#home-slider' ).on( 'cycle-post-initialize', function( ) {
            TweenMax.to($('#home-slider'), 1, {opacity: 1, ease: Quad.easeInOut});
        });

        $('#home-slider').cycle({
            speed: 600,
            manualSpeed: 600,
            timeout:  8000,
            slides: '> .slide',
            pager: '#home-slider-box .pager',
            pagerTemplate: '<span></span>',
            fx: 'scrollHorz'
        });
    }

};

jQuery(document).ready(function($) {

    xstudios.init($);

    $(window).resize(function() {
        xstudios.resize();
    });

});