<?php
    /*

    Template Name: Designer

     */
    get_header();

    global $woocommerce;

    $current_user = wp_get_current_user();

    if ($current_user == false) {
        wp_redirect(home_url());
    }

    $current_item_key = $_GET['cart_key'];
?>

<?php
    extract(etheme_get_page_sidebar());
?>

<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>

    <div class="page-heading bc-type-<?php echo esc_attr( etheme_get_option('breadcrumb_type') ); ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12 a-center">
                    <h1 class="title"><span><?php the_title(); ?></span></h1>
                    <?php etheme_breadcrumbs(); ?>
                </div>
            </div>
        </div>
    </div>

<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>
    <div class="page-heading-slider">
        <?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>
    </div>
<?php endif; ?>

    <div class="container content-page">
        <div class="sidebar-position-<?php echo esc_attr($position); ?> responsive-sidebar-<?php echo esc_attr($responsive); ?>">
            <div class="row">
                <?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
                    <div class="<?php echo esc_attr( $sidebar_span ); ?> sidebar sidebar-left">
                        <?php etheme_get_sidebar($sidebarname); ?>
                    </div>
                <?php endif; ?>

                <div class="content <?php echo esc_attr($content_span); ?>">
                    <div class="woocommerce-info"><strong>Please select Save and Share when you have completed your design. Thank you.</strong></div>
                    <?php if(have_posts()): while(have_posts()) : the_post(); ?>
                        <!-- Designer Start -->
                        <div>
                            <script type="text/javascript">
                                // Listen for the event.
                                window.document.addEventListener('designer_redirect', function (e) {
                                    console.log(e);
                                    window.location.href = '/cart';
                                }, false);
                            </script>
                            <script type="text/javascript" language="javascript" src="//stores.inksoft.com/designer/html5/common/js/launcher.js"></script>
                            <div align="center" id="embeddedDesigner"></div>
                            <script type="text/javascript" language="javascript">
                                var flashvars = {
                                    DesignerLocation: "//stores.inksoft.com/designer/html5",
                                    EnforceBoundaries: "1",
                                    Background: "",
                                    VectorOnly: false,
                                    DigitalPrint: true,
                                    ScreenPrint: true,
                                    Embroidery: false,
                                    MaxScreenPrintColors: "12",
                                    RoundPrices: false,
                                    StoreID: "2154",
                                    PublisherID: "775",
                                    SessionID: "",
                                    SessionToken: "",
                                    CartRetailItemID: "",
                                    UserID: "<?php echo the_author_meta( 'inksoft_user_id', $current_user->ID ); ?>",
                                    UserName: "",
                                    UserEmail: "<?php echo $current_user->user_email; ?>",
                                    DesignID: "",
                                    DefaultProductID: "<?php echo $_REQUEST['inksoft_sign_id']; ?>",
                                    DefaultProductStyleID: "<?php echo $_REQUEST['inksoft_sign_id']; ?>",
                                    ProductID: "<?php echo $_REQUEST['inksoft_sign_id']; ?>",
                                    ProductStyleID: "<?php echo $_REQUEST['inksoft_sign_id']; ?>",
                                    ProductCategoryID: "",
                                    ClipArtGalleryID: "",
                                    DisableAddToCart: true,
                                    DisableUploadImage: false,
                                    DisableClipArt: false,
                                    DisableUserArt: true,
                                    DisableProducts: true,
                                    DisableDesigns: false,
                                    DisableDistress: false,
                                    DisableResolutionMeter: true,
                                    DisableUploadVectorArt: false,
                                    DisableUploadRasterArt: false,
                                    StartPage: "Products",
                                    StartPageCategoryID: "",
                                    StartPageHTML: "",
                                    StartBanner: "",
                                    OrderID: "",
                                    CartID: "",
                                    ArtID: "",
                                    FontID: "",
                                    Domain: "stores.inksoft.com",
                                    SSLEnabled: false,
                                    SSLDomain: "",
                                    StoreURI: "HTHSigns",
                                    Admin: "",
                                    NextURL: "<?php echo get_template_directory_uri(); ?>/save-design.php?productID=<?php echo $_GET['product_id']; ?>&cartKey=<?php echo $_GET['cart_key']; ?>&designID=0",
                                    CartURL: "http://stores.inksoft.com/HTHSigns/Cart",
                                    OrderSummary: true,
                                    VideoLink: "http://www.youtube.com/watch?v=EfXICdRwt4E",
                                    Phone: "407-629-0012",
                                    WelcomeScreen: "",
                                    ContactUsLink: "/HTHSigns/Stores/Contact",
                                    WelcomeVideo: "",
                                    GreetBoxSetting: "LANDING",
                                    HelpVideoOverview: "",
                                    AutoZoom: false,
                                    EnableNameNumbers: true,
                                    AddThisPublisherId: "xa-4fccb0966fef0ba7",
                                    EnableCartPricing: false,
                                    EnableCartCheckout: false,
                                    EnableCartBilling: false,
                                    EnableCartShipping: false,
                                    PaymentDisabled: true,
                                    PaymentRequired: false,
                                    BillingAddressRequired: true,
                                    PasswordLength: "4",
                                    DefaultCountryCode: "US",
                                    CurrencyCode: "USD",
                                    CurrencySymbol: "$",
                                    HideProductPricing: true,
                                    PB: true,
                                    HideClipArtNames: true,
                                    HideDesignNames: true,
                                    ThemeName: "flat",
                                    FullScreen: false,
                                    Version: "4.3.0",
                                    BackgroundColor: "",
                                    StoreLogo: "//stores.inksoft.com/images/publishers/775/stores/HTHSigns/img/logo.png",
                                    StoreName: "HTH Inc.",
                                    EmbedType: "iframe"
                                };
                                launchDesigner('HTML5DS', flashvars, document.getElementById("embeddedDesigner"));
                            </script>
                            
                        </div>
                        <!-- /Designer End -->
                        

                        <div class="post-navigation">
                            <?php wp_link_pages(); ?>
                        </div>

                        <?php if ($post->ID != 0 && current_user_can('edit_post', $post->ID)): ?>
                            <?php edit_post_link( __('Edit this', ETHEME_DOMAIN), '<p class="edit-link">', '</p>' ); ?>
                        <?php endif ?>

                    <?php endwhile; else: ?>

                        <h3><?php _e('No pages were found!', ETHEME_DOMAIN) ?></h3>

                    <?php endif; ?>

                </div>

                <?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
                    <div class="<?php echo esc_attr($sidebar_span); ?> sidebar sidebar-right">
                        <?php etheme_get_sidebar($sidebarname); ?>
                    </div>
                <?php endif; ?>
            </div><!-- end row-fluid -->

        </div>
    </div><!-- end container -->

<?php
    get_footer();
?>
