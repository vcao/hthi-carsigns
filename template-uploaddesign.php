<?php
    /*

    Template Name: Upload Design

     */
    get_header();

    $current_user = wp_get_current_user();

    if ($current_user == false) {
        wp_redirect(home_url());
    }
?>

<?php
    extract(etheme_get_page_sidebar());
?>

<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>

    <div class="page-heading bc-type-<?php echo esc_attr( etheme_get_option('breadcrumb_type') ); ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12 a-center">
                    <h1 class="title"><span><?php the_title(); ?></span></h1>
                    <div class="breadcrumbs">
                        <a class="back-history" href="<?php echo get_permalink( $_GET['product_id'] ); ?>">Return to Product Page</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>
    <div class="page-heading-slider">
        <?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>
    </div>
<?php endif; ?>

    <div class="container content-page">
        <div class="sidebar-position-<?php echo esc_attr($position); ?> responsive-sidebar-<?php echo esc_attr($responsive); ?>">
            <div class="row">
                <?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
                    <div class="<?php echo esc_attr( $sidebar_span ); ?> sidebar sidebar-left">
                        <?php etheme_get_sidebar($sidebarname); ?>
                    </div>
                <?php endif; ?>

                <div class="content <?php echo esc_attr($content_span); ?>">
                    <div class="woocommerce-info"><strong>You are limited to 6 files. Please upload files that are 10mb or smaller. The accepted file types are .ai, .psd, .eps, .ai, .jpg, .png, and .gif.</strong></div>
                    <?php if(have_posts()): while(have_posts()) : the_post(); ?>
                        <!-- Upload Design Form Start -->

                        <form id="upload-files-form" enctype="multipart/form-data" method="post" action="<?php echo get_template_directory_uri(); ?>/save-design-files.php?productID=<?php echo $_GET['product_id']; ?>&cartKey=<?php echo $_GET['cart_key']; ?>&designID=0">
                            <input type="hidden" name="upload_files" value="true" />
                            <div class="row">
                                <div class="col-md-4">
                                    <h1><?php echo get_the_title( $_GET['product_id'] ); ?></h1>
                                    <?php echo get_the_post_thumbnail( $_GET['product_id'] ); ?>
                                </div>
                                <div class="col-md-8">
                                    <h2>Currently Uploaded Files</h2>

                                    <div class="uploaded-files">
                                        <?php if(isset($_SESSION['design_files'][$_GET['cart_key']])): ?>
                                            <?php
                                                $uploaded_files = $_SESSION['design_files'][$_GET['cart_key']];
                                                foreach ($uploaded_files as $file):
                                                    $file_url = $file['url'];
                                                    $file_path = $file['file'];
                                            ?>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <img src="<?php echo $file_url; ?>" alt="" />
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="file-box">
                                                            <div class="input-group file">
                                                                <input type="text" class="form-control file-input-label" value="<?php echo $file_url; ?>" data-path="<?php echo $file_path; ?>" data-cartkey="<?php echo $_GET['cart_key']; ?>" readonly>
                                                                <span class="input-group-btn">
                                                                    <button class="btn btn-secondary remove" type="button">Remove</button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                    <h2>Upload Design Files</h2>
                                    <div id="file-limit-notice" class=""></div>
                                    <div class="files">
                                        <div class="file-box">
                                            <div class="input-group file">
                                                <span class="input-group-btn">
                                                    <label class="btn btn-primary btn-file" >
                                                        <div class="input required"><input class="file-input" type="file" name="design_files[0]" accept="image/gif, image/png, image/jpeg, image/jpg, application/pdf, image/x-eps, image/psb, image/psd, .ai" required /></div> Browse Files
                                                    </label>
                                                </span>
                                                <input type="text" class="form-control file-input-label" placeholder="" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="add-file" class="btn btn-black filled button">Add File</div>

                                    <div class="row spacer-top">
                                        <div class="col-md-12">
                                            <button id="upload-btn" class="btn button big filled">Upload Files</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                        <!-- /Upload Design Form End -->
                        

                        <div class="post-navigation">
                            <?php wp_link_pages(); ?>
                        </div>

                        <?php if ($post->ID != 0 && current_user_can('edit_post', $post->ID)): ?>
                            <?php edit_post_link( __('Edit this', ETHEME_DOMAIN), '<p class="edit-link">', '</p>' ); ?>
                        <?php endif ?>

                    <?php endwhile; else: ?>

                        <h3><?php _e('No pages were found!', ETHEME_DOMAIN) ?></h3>

                    <?php endif; ?>

                </div>

                <?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
                    <div class="<?php echo esc_attr($sidebar_span); ?> sidebar sidebar-right">
                        <?php etheme_get_sidebar($sidebarname); ?>
                    </div>
                <?php endif; ?>
            </div><!-- end row-fluid -->

        </div>
    </div><!-- end container -->
    

    <!-- Handlebars Template -->
    <script id="file-template" type="text/x-handlebars-template">
        <div class="file-box">
            <div class="input-group file">
                <span class="input-group-btn">
                    <label class="btn btn-primary btn-file">
                        <div class="input required"><input class="file-input" type="file" name="design_files[]" accept="image/gif, image/png, image/jpeg, image/jpg, application/pdf, image/x-eps, image/psb, image/psd, .ai" required /></div> Browse Files
                    </label>
                </span>
                <input type="text" class="form-control file-input-label" placeholder="" readonly>
                <span class="input-group-btn">
                    <button class="btn btn-secondary remove" type="button">Remove</button>
                </span>
            </div>
        </div>
    </script>
<?php
    get_footer();
?>
