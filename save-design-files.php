<?php 
    require_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php'); 

    if ( ! function_exists( 'wp_handle_upload' ) ) {
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
    }

    global $woocommerce;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
</head>
<?php

    $items = $woocommerce->cart->get_cart();

    error_log(print_r($items, 1));

    function unique_filename($dir, $name, $ext)
    {
        return $name."_".time().$ext;
    }

    foreach ($items as $key => $values) {
        $_productPost = $values['data']->post;
        if ($values['product_id'] == $_GET['productID']) {

            if (!isset($_SESSION['design_files'])) {
                $_SESSION['design_files'] = array();
            }
            
            if (!empty($_SESSION['design_files'])) {

                // error_log("UPLOADED FILES:");
                // error_log("===============");
                // error_log(print_r($_FILES, 1));

                $file_data = array();

                foreach ($_FILES as $key => $file) {

                    $file_ext = pathinfo($file['name'][0], PATHINFO_EXTENSION);
                    $file_name = pathinfo($file['name'][0], PATHINFO_FILENAME)."_".time().".".$file_ext;

                    $file = wp_upload_bits( $file_name, null, file_get_contents( $file['tmp_name'][0] ) );

                    if ($file['error'] == false) {
                        error_log("File is valid, and was successfully uploaded.\n");
                        error_log(print_r($file, 1));

                        $file_data[] = $file;
                    } else {
                        error_log("File has error.\n");
                        error_log(print_r($file, 1));
                    }
                }
                
                if (!empty($_SESSION['design_files'][$_GET['cartKey']])) {

                    foreach ($file_data as $key => $new_file) {
                       $_SESSION['design_files'][$_GET['cartKey']][] = $new_file;
                    }

                    
                } else {
                    $_SESSION['design_files'][$_GET['cartKey']] = array();

                    $_SESSION['design_files'][$_GET['cartKey']] = $file_data;
                }
                
            } else {

                // error_log("UPLOADED FILES:");
                // error_log("===============");
                // error_log(print_r($_FILES, 1));

                $file_data = array();

                foreach ($_FILES['design_files'] as $key => $file) {
                    
                    $file_ext = pathinfo($file['name'][0], PATHINFO_EXTENSION);
                    $file_name = pathinfo($file['name'][0], PATHINFO_FILENAME)."_".time().".".$file_ext;

                    $file = wp_upload_bits( $file_name, null, file_get_contents( $file['tmp_name'][0] ) );

                    if ($file['error'] == false) {
                        error_log("File is valid, and was successfully uploaded.\n");
                        error_log(print_r($file, 1));

                        $file_data[] = $file;
                    } else {
                        error_log("File has error.\n");
                        error_log(print_r($file, 1));
                    }
                }

                $_SESSION['design_files'] = array();

                $_SESSION['design_files'][$_GET['cartKey']] = array();

                $_SESSION['design_files'][$_GET['cartKey']] = $file_data;

            }
 
        }
    }

?>
<body>
    <script type="text/javascript">

        window.location.href = '/cart';

    </script>
</body>
</html>