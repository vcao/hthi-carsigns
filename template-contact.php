<?php 
    /*

    Template Name: Contact

     */
    get_header();
?>

<?php 
	extract(etheme_get_page_sidebar());
?>

<?php if ($page_heading != 'disable' && ($page_slider == 'no_slider' || $page_slider == '')): ?>
	
	<div class="page-heading bc-type-<?php echo esc_attr( etheme_get_option('breadcrumb_type') ); ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12 a-center">
					<h1 class="title"><span><?php the_title(); ?></span></h1>
					<?php etheme_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	</div>

<?php endif ?>

<?php if($page_slider != 'no_slider' && $page_slider != ''): ?>
	<div class="page-heading-slider">
		<?php echo do_shortcode('[rev_slider_vc alias="'.$page_slider.'"]'); ?>
	</div>
<?php endif; ?>

	<div class="container content-page">
		<div class="sidebar-position-<?php echo esc_attr($position); ?> responsive-sidebar-<?php echo esc_attr($responsive); ?>">
			<div class="row">
				<?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
					<div class="<?php echo esc_attr( $sidebar_span ); ?> sidebar sidebar-left">
						<?php etheme_get_sidebar($sidebarname); ?>
					</div>
				<?php endif; ?>
				<div></div>
				<div class="content <?php echo esc_attr($content_span); ?>">
					<div id="map"></div>
					<?php if(isset($_GET['result'])): ?>

						<?php if($_GET['result'] == true): ?>
							<ul class="alert-success">
								<li>Your message was successfully sent. We will respond shortly. Thank you!</li>
							</ul>
						<?php endif; ?>

						<?php if($_GET['result'] == false): ?>
							<ul class="alert-error">
								<li>An error occurred. Please try to send your message again.</li>
							</ul>
						<?php endif; ?>
					<?php endif; ?>
					<form id="contact-form" action="<?php echo get_template_directory_uri(); ?>/framework/process-contact.php" method="post">
						<div class="row">
							<div class="col-md-6 input-box">
								<label>Name (Required)</label>
								<input name="name" class="form-control" required>
							</div>
							<div class="col-md-6 input-box">
								<label>Email (Required)</label>
								<input name="email" class="form-control" required>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 input-box">
								<label>Phone</label>
								<input name="phone" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 input-box">
								<label>Message (Required)</label>
								<textarea name="message" class="form-control" required></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 input-box">
								<input type="submit" value="submit message" class="btn big btn-black" />
							</div>
						</div>
						
					</form>

				</div>

				<?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
					<div class="<?php echo esc_attr($sidebar_span); ?> sidebar sidebar-right">
						<?php etheme_get_sidebar($sidebarname); ?>
					</div>
				<?php endif; ?>
			</div><!-- end row-fluid -->

		</div>
	</div><!-- end container -->
	
	<script type="text/javascript">
		// Google Maps API Key
		//var api_key = 'AIzaSyA-OsJA87x1Gc4XFOsJg8qIb9KSCDzGAb8';
		var map;
		var lat_lng = {lat: 28.591383, lng: -81.371242};
		var curr_center = {lat: 28.591383, lng: -81.371242};
		var path = "<?php echo get_template_directory_uri(); ?>";
		function initMap() {

			//offset
			var offsetY = lat_lng.lat*0.001;

		  	map = new google.maps.Map(document.getElementById('map'), {
		    	center: lat_lng,
		    	zoom: 19
		  	});

		  	curr_center = map.getCenter();

		  	var marker = new google.maps.Marker({
			    position: lat_lng,
			    map: map,
			    title: 'Hello World!',
			    visible: true
			});
			marker.setMap(map);

			var contentString = '<div id="map-info">'+
			'<a href="https://maps.google.com/maps?q=711+Jackson+Ave++Winter+Park,+%C2%A0FL+32789&amp;ie=UTF8&amp;hnear=711+Jackson+Ave,+Winter+Park,+Orange,+Florida+32789&amp;gl=us&amp;t=m&amp;z=16&amp;iwloc=A">'+
			'<img class="logo" src="'+path+'/images/footer-logo.png"><p>711 Jackson Ave<br>Winter Park, FL 32789</p></a>'+
			'</div>'
			;

			var infowindow = new google.maps.InfoWindow({
				content: contentString,
				maxWidth: 200
			});

			infowindow.open(map, marker);

			marker.addListener('click', function() {
    			infowindow.open(map, marker);
  			});

			window.addEventListener("resize", function(){
				google.maps.event.trigger(map, 'resize');
				map.setCenter(curr_center);
			});
		}

	</script>
	<script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-OsJA87x1Gc4XFOsJg8qIb9KSCDzGAb8&callback=initMap">
    </script>

<?php
	get_footer();
?>
