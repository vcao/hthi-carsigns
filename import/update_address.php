<?php

require_once("../../../../wp-load.php");

global $wpdb;

function csv_to_array($file)
{
    $array = $fields = array(); $i = 0;
    $handle = @fopen($file, "r");
    if ($handle) {
        while (($row = fgetcsv($handle, 0)) !== false) {
            if (empty($fields)) {
                $fields = $row;
                continue;
            }
            foreach ($row as $k=>$value) {
                $array[$i][$fields[$k]] = $value;
            }
            $i++;
        }
        if (!feof($handle)) {
            echo "Error: unexpected fgets() fail\n";
        }
        fclose($handle);
    }
    return $array;
}

function get_state_abbr($state='')
{
    $states = array(
        'AL'=>'Alabama',
        'AK'=>'Alaska',
        'AZ'=>'Arizona',
        'AR'=>'Arkansas',
        'CA'=>'California',
        'CO'=>'Colorado',
        'CT'=>'Connecticut',
        'DE'=>'Delaware',
        'DC'=>'District of Columbia',
        'FL'=>'Florida',
        'GA'=>'Georgia',
        'HI'=>'Hawaii',
        'ID'=>'Idaho',
        'IL'=>'Illinois',
        'IN'=>'Indiana',
        'IA'=>'Iowa',
        'KS'=>'Kansas',
        'KY'=>'Kentucky',
        'LA'=>'Louisiana',
        'ME'=>'Maine',
        'MD'=>'Maryland',
        'MA'=>'Massachusetts',
        'MI'=>'Michigan',
        'MN'=>'Minnesota',
        'MS'=>'Mississippi',
        'MO'=>'Missouri',
        'MT'=>'Montana',
        'NE'=>'Nebraska',
        'NV'=>'Nevada',
        'NH'=>'New Hampshire',
        'NJ'=>'New Jersey',
        'NM'=>'New Mexico',
        'NY'=>'New York',
        'NC'=>'North Carolina',
        'ND'=>'North Dakota',
        'OH'=>'Ohio',
        'OK'=>'Oklahoma',
        'OR'=>'Oregon',
        'PA'=>'Pennsylvania',
        'RI'=>'Rhode Island',
        'SC'=>'South Carolina',
        'SD'=>'South Dakota',
        'TN'=>'Tennessee',
        'TX'=>'Texas',
        'UT'=>'Utah',
        'VT'=>'Vermont',
        'VA'=>'Virginia',
        'WA'=>'Washington',
        'WV'=>'West Virginia',
        'WI'=>'Wisconsin',
        'WY'=>'Wyoming',
    );

    foreach ($states as $skey => $s) {
        if ($state == $s) {
            return $skey;
        }
    }
}



# Import customers
$customers_file = get_template_directory().'/import/'.'export_customers.csv';
$import_users = csv_to_array($customers_file);

$current_users = $wpdb->get_results( 'SELECT ID, `wp_users`.`user_email`, (SELECT `meta_value` FROM wp_usermeta WHERE `meta_key` = "billing_address_1" AND `wp_usermeta`.`user_id` = `wp_users`.`ID`) as billing_address_1 FROM wp_users LEFT JOIN wp_usermeta ON `wp_users`.`ID` = `wp_usermeta`.`user_id` GROUP BY ID;', ARRAY_A );


foreach ($current_users as $cKey => $cUser) {

    foreach ($import_users as $iKey => $iUser) {

        if ($cUser['user_email'] == $iUser['email']) {

            if ($cUser['billing_address_1'] != $iUser['billing_street1']) {
                echo '<p>Email: '.$cUser['user_email'].'</p>';
                echo '<p>'.$cUser['billing_address_1'].'</p>';
                echo '<p>Address Update Needed:</p>';

                // Address
                // ------------------------------
                
                $billing_address_1=$wpdb->update(
                    'wp_usermeta',
                    array('meta_value'=>$iUser['billing_street1'],),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'billing_address_1'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $billing_address_1) {
                    echo '<p>FAILED UPDATE: billing_address_1</p>';
                }

                $shipping_address_1 = $wpdb->update( 
                    'wp_usermeta',
                    array('meta_value'=>$iUser['shipping_street1'],),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'shipping_address_1'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $shipping_address_1) {
                    echo '<p>FAILED UPDATE: shipping_address_1</p>';
                }

                // City
                // ------------------------------

                $billing_city = $wpdb->update( 
                    'wp_usermeta',
                    array('meta_value'=>$iUser['billing_city'],),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'billing_city'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $billing_city) {
                    echo '<p>FAILED UPDATE: billing_city</p>';
                }

                $shipping_city = $wpdb->update( 
                    'wp_usermeta',
                    array('meta_value'=>$iUser['shipping_city'],),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'shipping_city'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $shipping_city) {
                    echo '<p>FAILED UPDATE: shipping_city</p>';
                }

                // State
                // ------------------------------

                $billing_state = $wpdb->update( 
                    'wp_usermeta',
                    array('meta_value'=>get_state_abbr($iUser['billing_region'])),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'billing_state'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $billing_state) {
                    echo '<p>FAILED UPDATE: billing_state</p>';
                }

                $shipping_state = $wpdb->update( 
                    'wp_usermeta',
                    array('meta_value'=>get_state_abbr($iUser['shipping_region'])),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'shipping_state'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $shipping_state) {
                    echo '<p>FAILED UPDATE: shipping_state</p>';
                }

                // Postcode
                // ------------------------------

                $billing_postcode = $wpdb->update( 
                    'wp_usermeta',
                    array('meta_value'=> $iUser['billing_postcode'],),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'billing_postcode'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $billing_postcode) {
                    echo '<p>FAILED UPDATE: billing_postcode</p>';
                }

                $shipping_postcode = $wpdb->update( 
                    'wp_usermeta',
                    array('meta_value'=> $iUser['shipping_postcode'],),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'shipping_postcode'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $shipping_postcode) {
                    echo '<p>FAILED UPDATE: shipping_postcode</p>';
                }

                // Phone
                // ------------------------------

                $billing_phone = $wpdb->update( 
                    'wp_usermeta',
                    array('meta_value'=> $iUser['billing_telephone'],),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'billing_phone'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $billing_phone) {
                    echo '<p>FAILED UPDATE: billing_phone</p>';
                }

                // Company
                // ------------------------------

                $billing_company = $wpdb->update( 
                    'wp_usermeta',
                    array('meta_value'=> $iUser['billing_company'],),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'billing_company'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $billing_company) {
                    echo '<p>FAILED UPDATE: billing_company</p>';
                }

                $shipping_company = $wpdb->update( 
                    'wp_usermeta',
                    array('meta_value'=> $iUser['shipping_company'],),
                    array('user_id'=>$cUser['ID'],'meta_key'=>'shipping_company'),
                    array('%s',),
                    array('%d','%s',)
                );

                if (false === $shipping_company) {
                    echo '<p>FAILED UPDATE: shipping_company</p>';
                }
               

                echo '<p><hr /></p>';
            }

            break;
        }
    }
    

    

    
}

