<?php 
    require_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php'); 

    global $woocommerce;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
</head>
<?php

    $items = $woocommerce->cart->get_cart();

    foreach ($items as $key => $values) {
        $_productPost = $values['data']->post;
        if ($values['product_id'] == $_GET['productID']) {

            if (!empty($_SESSION['designs'])) {

                if (!empty($_SESSION['designs'][$_GET['cartKey']])) {
                    $_SESSION['designs'][$_GET['cartKey']] = array(
                        'product_id' => $_GET['productID'],
                        'design_id' => $_GET['designID']
                    );
                } else {
                    $_SESSION['designs'][$_GET['cartKey']] = array(
                        'product_id' => $_GET['productID'],
                        'design_id' => $_GET['designID']
                    );
                }
                
            } else {
                $_SESSION['designs'] = array();

                $_SESSION['designs'][$_GET['cartKey']] = array(
                    'product_id' => $_GET['productID'],
                    'design_id' => $_GET['designID']
                );

            }
 
        }
    }
?>
<body>
    <script type="text/javascript">

        var event = document.createEvent('Event');
        event.initEvent('designer_redirect', true, true);
        event.design_id = "<?php echo $_GET['designID']; ?>";
        event.product_id = "<?php echo $_GET['productID']; ?>";
        // Dispatch the event.
        window.parent.document.dispatchEvent(event);
    </script>
</body>
</html>