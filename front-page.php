<?php

    get_header();
?>

<div class="container content-page">
    <?php $position = isset($position) ? $position : ""; ?>
    <?php $responsive = isset($responsive) ? $responsive : ""; ?>
    <?php $content_span = isset($content_span) ? $content_span : ""; ?>
    <div class="sidebar-position-<?php echo esc_attr(isset($position) ? $position : ""); ?> responsive-sidebar-<?php echo esc_attr(isset($position) ? $position : ""); ?>">
        <div class="row">
            <?php if($position == 'left' || ($responsive == 'top' && $position == 'right')): ?>
                <div class="<?php echo esc_attr( $sidebar_span ); ?> sidebar sidebar-left">
                    <?php etheme_get_sidebar($sidebarname); ?>
                </div>
            <?php endif; ?>

            <div class="col-md-3">
                <div id="home-categories">
                    <h2 class="title">SHOP BY CATEGORY</h2>
                    <?php
                        
                        $args = array(
                            'taxonomy'     => 'product_cat',
                            'orderby'      => 'count',
                            'order' => 'DESC',
                            'show_count'   => 0,
                            'pad_counts'   => 0,
                            'hierarchical' => 1,
                            'title_li'     => '',
                            'hide_empty'   => 0,
                            'exclude' => ''
                        );
                        ?>
                        <?php $all_categories = get_categories( $args );
                        //print_r($all_categories);
                        foreach ($all_categories as $cat) {
                        //print_r($cat);
                        
                        // Get category metadata
                        $term_meta = get_metadata( 'taxonomy', $cat->term_id, 'cat_meta', 1 );

                        // Skip hidden fields
                        if (!empty($term_meta['hidden'])) {
                            continue;
                        }

                        if($cat->category_parent == 0) {
                            $category_id = $cat->term_id;
                        ?>      

                        <?php       

                            echo '<a class="category" href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>'; ?>


                            <?php
                            $args2 = array(
                              'taxonomy'     => 'product_cat',
                              'child_of'     => 0,
                              'parent'       => $category_id,
                              'orderby'      => 'count',
                              'show_count'   => 0,
                              'pad_counts'   => 0,
                              'hierarchical' => 0,
                              'title_li'     => '',
                              'hide_empty'   => 0
                            );
                            $sub_cats = get_categories( $args2 );
                            if(count($sub_cats) > 0) {
                                foreach($sub_cats as $sub_category) {
                                    //echo $sub_category->name ;
                                }

                            } ?>



                        <?php }     
                        }
                    ?>  
                </div>
            </div>
            <div class="col-md-9">
                <div id="home-slider-box">
                    <div id="home-slider">
                        <?php

                            // check if the repeater field has rows of data
                            if( have_rows('slides') ):

                                // loop through the rows of data
                                while ( have_rows('slides') ) : the_row();
                        ?>
                                    <div class="slide">
                                        <a href="<?php the_sub_field('url'); ?>"><img class="" src="<?php the_sub_field('image'); ?>" /></a>
                                    </div>
                        <?php

                                endwhile;

                            endif;

                        ?>
                    </div>
                     <div class="pager"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h2 class="title-center"><span>TOP SELLERS</span></h2>
                <?php echo do_shortcode('[featured_products per_page="4" columns="4"]'); ?>
            </div>
        </div>
    </div>
</div><!-- end container -->

<div class="container about">
        <div class="row about">
            <div class="col-md-2">
                <img class="" src="<?php echo get_template_directory_uri(); ?>/images/hth-badge.png" />
            </div>
            <div class="col-md-10">
                <h2>ABOUT US</h2>
                <p class="subtitle">THE ORIGINAL CAR SIGN MAKER SINCE 1984</p>
                <div class="line"></div>
                <p class="desc"><b>Choose the HTH Inc. car top sign and choose the most recognized quality car top sign available.</b> Our durable signs are the industry standard. While they are often imitated, only our long lasting, aerodynamic signs provide the best return on investment for advertising your business making them the highest quality car signs since 1984.</p>
            </div>
        </div>
</div><!-- end container -->

<div class="container quality">
        <div class="row">
            <div class="col-md-12">
                <h2>SIGNS BUILT TO LAST</h2>
                <p class="subtitle">THE INDUSTRY STANDARD ON QUALITY AND DURABILITY</p>
                <div class="line"></div>
            </div>
            <div class="col-md-4">
                <ul>
                    <li>One-piece, molded construction for durability</li>
                    <li>Durable polyethylene virtually eliminates breakage</li>
                    <li>Bright lighting system for unmatched visibility day and night</li>
                    <li>Laminated printed message</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li>Soft-coated magnet pads prevent scrapes on vehicles</li>
                    <li>U/V protected to resist fading and yellowing from sun</li>
                    <li>Stainless steel hardware that won’t rust</li>
                    <li>Aerodynamic design for maximum airflow efficiency</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li>Neodymium rare earth magnets for maximum strength</li>
                    <li>Scratch-proof and wipes clean</li>
                    <li>Durable cords with extra insulation on wires and ABS plastic on plugs</li>
                    <li>Rechargeable LED Battery Light System Optional</li>
                </ul>
            </div>
            <div class="col-md-12 call">
                <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ) ?>" class="btn-shop">SHOP FOR CAR SIGNS</a>
                <p class="number">Or call <a href="tel:18003211850">1.800.321.1850</a></p>
            </div>
        </div>
</div><!-- end container -->

<div class="container">

        <div class="row">

            <div class="content <?php echo esc_attr($content_span); ?>">
                <?php if(have_posts()): while(have_posts()) : the_post(); ?>
                    
                    <?php the_content(); ?>

                    <div class="post-navigation">
                        <?php wp_link_pages(); ?>
                    </div>
                    
                    <?php if ($post->ID != 0 && current_user_can('edit_post', $post->ID)): ?>
                        <?php edit_post_link( __('Edit this', ETHEME_DOMAIN), '<p class="edit-link">', '</p>' ); ?>
                    <?php endif ?>

                <?php endwhile; else: ?>

                    <h3><?php _e('No pages were found!', ETHEME_DOMAIN) ?></h3>

                <?php endif; ?>

            </div>

            <?php if($position == 'right' || ($responsive == 'bottom' && $position == 'left')): ?>
                <div class="<?php echo esc_attr($sidebar_span); ?> sidebar sidebar-right">
                    <?php etheme_get_sidebar($sidebarname); ?>
                </div>
            <?php endif; ?>
        </div><!-- end row-fluid -->

    </div>
</div><!-- end container -->

<?php

    get_footer();

?>