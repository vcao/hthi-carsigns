<?php
/**
 * Single Product Meta
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span>.</span>

	<?php endif; ?>

    <span class="posted_in">
        <?php echo $cat_count > 1 ? 'Categories:' : 'Category:'; ?>
        <?php
            $size = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
            $product_cats = explode(',', $product->get_categories( ', ' ));

            $args = array(
                'taxonomy'     => 'product_cat',
                'orderby'      => 'count',
                'order' => 'DESC',
                'show_count'   => 0,
                'pad_counts'   => 0,
                'hierarchical' => 1,
                'title_li'     => '',
                'hide_empty'   => 0,
                'exclude' => ''
            );

            $all_categories = get_the_terms( $post->ID, 'product_cat' ) ;
            //print_r($all_categories);
            foreach ($all_categories as $cat) {
                // Get category metadata
                $term_meta = get_metadata( 'taxonomy', $cat->term_id, 'cat_meta', 1 );

                // Skip hidden fields
                if (!empty($term_meta['hidden'])) {
                    foreach ($product_cats as $cat_key => $prod_cat) {
                        if (stripos($prod_cat, $cat->name)) {
                            unset($product_cats[$cat_key]);
                        }
                    }
                }
            }

            echo implode(',', $product_cats);
        ?>
    </span>

	<?php echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '.</span>' ); ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
