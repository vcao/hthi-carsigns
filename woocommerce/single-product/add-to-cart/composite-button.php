<?php
/**
 * Composite add-to-cart button template.
 *
 * Override this template by copying it to 'yourtheme/woocommerce/single-product/add-to-cart/composite-button.php'.
 *
 * On occasion, this template file may need to be updated and you (the theme developer) will need to copy the new files to your theme to maintain compatibility.
 * We try to do this as little as possible, but it does happen.
 * When this occurs the version of the template file will be bumped and the readme will list any important changes.
 *
 * @version 2.5.4
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

?>
<div class="buttons-box">
    <!--<button type="submit" class="single_add_to_cart_button composite_add_to_cart_button button alt"></button>-->

    <button type="submit" id="upload_files_button" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
    
    <?php $_inksoft_sign_id = get_post_meta( $product->id, '_inksoft_sign_id', 1 ); if(!empty($_inksoft_sign_id)): ?>
            
        <?php endif; ?>
    </div>
</div>