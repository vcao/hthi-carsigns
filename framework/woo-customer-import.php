<?php

function csv_to_array($file)
{
    $array = $fields = array(); $i = 0;
    $handle = @fopen($file, "r");
    if ($handle) {
        while (($row = fgetcsv($handle, 0)) !== false) {
            if (empty($fields)) {
                $fields = $row;
                continue;
            }
            foreach ($row as $k=>$value) {
                $array[$i][$fields[$k]] = $value;
            }
            $i++;
        }
        if (!feof($handle)) {
            echo "Error: unexpected fgets() fail\n";
        }
        fclose($handle);
    }
    return $array;
}

function get_state_abbr($state='')
{
    $states = array(
        'AL'=>'Alabama',
        'AK'=>'Alaska',
        'AZ'=>'Arizona',
        'AR'=>'Arkansas',
        'CA'=>'California',
        'CO'=>'Colorado',
        'CT'=>'Connecticut',
        'DE'=>'Delaware',
        'DC'=>'District of Columbia',
        'FL'=>'Florida',
        'GA'=>'Georgia',
        'HI'=>'Hawaii',
        'ID'=>'Idaho',
        'IL'=>'Illinois',
        'IN'=>'Indiana',
        'IA'=>'Iowa',
        'KS'=>'Kansas',
        'KY'=>'Kentucky',
        'LA'=>'Louisiana',
        'ME'=>'Maine',
        'MD'=>'Maryland',
        'MA'=>'Massachusetts',
        'MI'=>'Michigan',
        'MN'=>'Minnesota',
        'MS'=>'Mississippi',
        'MO'=>'Missouri',
        'MT'=>'Montana',
        'NE'=>'Nebraska',
        'NV'=>'Nevada',
        'NH'=>'New Hampshire',
        'NJ'=>'New Jersey',
        'NM'=>'New Mexico',
        'NY'=>'New York',
        'NC'=>'North Carolina',
        'ND'=>'North Dakota',
        'OH'=>'Ohio',
        'OK'=>'Oklahoma',
        'OR'=>'Oregon',
        'PA'=>'Pennsylvania',
        'RI'=>'Rhode Island',
        'SC'=>'South Carolina',
        'SD'=>'South Dakota',
        'TN'=>'Tennessee',
        'TX'=>'Texas',
        'UT'=>'Utah',
        'VT'=>'Vermont',
        'VA'=>'Virginia',
        'WA'=>'Washington',
        'WV'=>'West Virginia',
        'WI'=>'Wisconsin',
        'WY'=>'Wyoming',
    );

    foreach ($states as $skey => $s) {
        if ($state == $s) {
            return $skey;
        }
    }
}



# Import customers
$customers_file = get_template_directory().'/import/'.'magento_customers_2015_192.155.93.119_1-27-16_5-15 PM.csv';
$customers = csv_to_array($customers_file);

function import_customers($customers)
{
    foreach($customers as $ckey => $customer){
        //$user_id = wc_create_new_customer( $customer['email'], $customer['email'], $password );
        $user_id = wc_create_new_customer( $customer['email'], $customer['email'], 'newmedia' );

        if (is_int($user_id)) {

            # Magento Entity ID
            add_user_meta( $user_id, 'entity_id', $customer['entity_id'], 1 );

            # Profile Information
            update_user_meta( $user_id, "first_name", $customer['firstname'] );
            update_user_meta( $user_id, "last_name", $customer['lastname'] );

            # Billing Information
            update_user_meta( $user_id, "billing_first_name", $customer['firstname'] );
            update_user_meta( $user_id, "billing_last_name", $customer['lastname'] );
            update_user_meta( $user_id, "billing_company", $customer['company'] );
            update_user_meta( $user_id, "billing_address_1", $customer['street'] );
            update_user_meta( $user_id, "billing_address_2", '' );
            update_user_meta( $user_id, "billing_city", $customer['city'] );
            update_user_meta( $user_id, "billing_postcode", $customer['zip'] );
            update_user_meta( $user_id, "billing_country", $customer['country']  );
            update_user_meta( $user_id, "billing_state", get_state_abbr($customer['state']) );
            update_user_meta( $user_id, "billing_email", $customer['email'] );
            update_user_meta( $user_id, "billing_phone", $customer['phone'] );

            # Shipping Information
            update_user_meta( $user_id, "shipping_first_name", $customer['firstname'] );
            update_user_meta( $user_id, "shipping_last_name", $customer['lastname'] );
            update_user_meta( $user_id, "shipping_company", $customer['company'] );
            update_user_meta( $user_id, "shipping_address_1", $customer['street'] );
            update_user_meta( $user_id, "shipping_address_2", '' );
            update_user_meta( $user_id, "shipping_city", $customer['city'] );
            update_user_meta( $user_id, "shipping_postcode", $customer['zip'] );
            update_user_meta( $user_id, "shipping_country", $customer['country']  );
            update_user_meta( $user_id, "shipping_state", get_state_abbr($customer['state']) );

            // Create InkSoft User
            $fields = array(
                'E' => urlencode($customer['email']),
                'P' => urlencode(time()),
                'FN' => urlencode($customer['firstname']),
                'LN' => urlencode($customer['lastname'])
            );

            //url-ify the data for the POST
            $fields_string = "";
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string, '&');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://stores.inksoft.com/HTHSigns/QuickUserAdd775-2154");
            curl_setopt($ch, CURLOPT_FAILONERROR,1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            $retValue = curl_exec($ch);          
            curl_close($ch);

            $retXML = new SimpleXMLElement($retValue);
            $retJson = json_encode($retXML);
            $retArray = json_decode($retJson,TRUE);

            if (strpos($retValue, 'UserID') !== false) {
                update_user_meta( $user_id, 'inksoft_user_id', $retArray[0] );
            }

        }

    }
}

//import_customers($customers);




