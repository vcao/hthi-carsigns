<?php

// Register Custom Post Type
function testimonial() {

    $labels = array(
        'name'                  => _x( 'Testimonials', 'Post Type General Name', 'testimonial' ),
        'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'testimonial' ),
        'menu_name'             => __( 'Testimonials', 'testimonial' ),
        'name_admin_bar'        => __( 'Testimonials', 'testimonial' ),
        'archives'              => __( 'Testimonial Archives', 'testimonial' ),
        'parent_item_colon'     => __( 'Parent Testimonial:', 'testimonial' ),
        'all_items'             => __( 'All Testimonials', 'testimonial' ),
        'add_new_item'          => __( 'Add New Testimonial', 'testimonial' ),
        'add_new'               => __( 'Add New', 'testimonial' ),
        'new_item'              => __( 'New Testimonial', 'testimonial' ),
        'edit_item'             => __( 'Edit Testimonial', 'testimonial' ),
        'update_item'           => __( 'Update Testimonial', 'testimonial' ),
        'view_item'             => __( 'View Testimonial', 'testimonial' ),
        'search_items'          => __( 'Search Testimonial', 'testimonial' ),
        'not_found'             => __( 'Not found', 'testimonial' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'testimonial' ),
        'featured_image'        => __( 'Testimonial Image', 'testimonial' ),
        'set_featured_image'    => __( 'Set Testimonial image', 'testimonial' ),
        'remove_featured_image' => __( 'Remove Testimonial image', 'testimonial' ),
        'use_featured_image'    => __( 'Use as Testimonial image', 'testimonial' ),
        'insert_into_item'      => __( 'Insert into Testimonial', 'testimonial' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'testimonial' ),
        'items_list'            => __( 'Testimonials list', 'testimonial' ),
        'items_list_navigation' => __( 'Testimonials list navigation', 'testimonial' ),
        'filter_items_list'     => __( 'Filter Testimonials list', 'testimonial' ),
    );
    $args = array(
        'label'                 => __( 'Testimonial', 'testimonial' ),
        'description'           => __( 'Customer Testimonials', 'testimonial' ),
        'labels'                => $labels,
        'supports'              => array( 'editor', 'author', 'revisions', ),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-thumbs-up',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,       
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'testimonial', $args );

}
add_action( 'init', 'testimonial', 0 );


// ADD NEW COLUMN
function testimonial_columns_head($defaults) {
    $defaults['name'] = 'Name';


    return $defaults;
}
 
// SHOW THE FEATURED IMAGE
function testimonial_columns_content($column_name, $post_ID) {
    if ($column_name == 'name') {
        ?>
        <a class="row-title" href="<?= admin_url( 'post.php?post='.$post_ID.'&action=edit' ); ?>"><?= get_field('name', $post_ID); ?></a>
        <?php
    }
}

add_filter('manage_testimonial_posts_columns', 'testimonial_columns_head');
add_action('manage_testimonial_posts_custom_column', 'testimonial_columns_content', 10, 2);

// REMOVE DEFAULT CATEGORY COLUMN
function testimonial_columns_remove_category($defaults) {
    // to get defaults column names:
    // print_r($defaults);
    unset($defaults['title']);
    unset($defaults['author']);
    unset($defaults['date']);
    return $defaults;
}

add_filter('manage_testimonial_posts_columns', 'testimonial_columns_remove_category');
