<?php


/**
* Posts Import
*/
class Post_Import
{

    private $posts_file = "";
    private $user_ID;
    
    function __construct($csv)
    {
        if (empty($csv)) {
            throw new Exception("The csv import file is required.", 1);    
        } else {
            $this->posts_file = $csv;
            $this->user_ID = get_current_user_id();
        }
    }

    private function csv_to_array($file)
    {
        $array = $fields = array(); $i = 0;
        $handle = @fopen($file, "r");
        if ($handle) {
            while (($row = fgetcsv($handle, 0)) !== false) {
                if (empty($fields)) {
                    $fields = $row;
                    continue;
                }
                foreach ($row as $k=>$value) {
                    $array[$i][$fields[$k]] = $value;
                }
                $i++;
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        }
        return $array;
    }

    private function delete_all_posts()
    {
        $args = array(
            'post_type'   => 'post',
            'numberposts' => -1
        );
        $posts = get_posts( $args );

        foreach ($posts as $post) {
            wp_delete_post($post->ID, 1);
        }
    }

    private function slugify($title)
    {
        $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $title);
        return strtolower($slug);
    }

    public function import()
    {
        # Delete Posts
        //self::delete_all_posts();
        
        # Get Current Post Titles
        $args = array(
            'post_type'   => 'post',
            'numberposts' => -1
        );
        $current_posts = get_posts( $args );
        $post_titles = array();

        foreach ($current_posts as $post) {
            $post_titles[] = $post->post_date;
        }


        # Import posts
        $posts = self::csv_to_array($this->posts_file);

        if ($this->user_ID) {
            foreach($posts as $pkey => $post){
                
                if (!in_array($post['created_time'], $post_titles)) {
                    $new_post = array(
                        'post_title'    => $post['title'],
                        'post_content'  => $post['post_content'],
                        'post_status'   => 'publish',
                        'post_date'     => $post['created_time'],
                        'post_author'   => $this->user_ID,
                        'post_type'     => 'post',
                        'post_category' => array(0)
                    );
                    $post_id = wp_insert_post( $new_post );
                }
            }
        }   

        return true;
    }
}

$post_import = new Post_Import(get_template_directory().'/import/'.'carsigns_oldposts.csv');
//$import_result = $post_import->import();








