<?php

// Register Custom Post Type
function resource() {

    $labels = array(
        'name'                  => _x( 'Resources', 'Post Type General Name', 'resource' ),
        'singular_name'         => _x( 'Resource', 'Post Type Singular Name', 'resource' ),
        'menu_name'             => __( 'Resources', 'resource' ),
        'name_admin_bar'        => __( 'Resource', 'resource' ),
        'archives'              => __( 'Resource Archives', 'resource' ),
        'parent_item_colon'     => __( 'Parent Resource:', 'resource' ),
        'all_items'             => __( 'All Resources', 'resource' ),
        'add_new_item'          => __( 'Add New Resource', 'resource' ),
        'add_new'               => __( 'Add New', 'resource' ),
        'new_item'              => __( 'New Resource', 'resource' ),
        'edit_item'             => __( 'Edit Resource', 'resource' ),
        'update_item'           => __( 'Update Resource', 'resource' ),
        'view_item'             => __( 'View Resource', 'resource' ),
        'search_items'          => __( 'Search Resource', 'resource' ),
        'not_found'             => __( 'Not found', 'resource' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'resource' ),
        'featured_image'        => __( 'Featured Image', 'resource' ),
        'set_featured_image'    => __( 'Set featured image', 'resource' ),
        'remove_featured_image' => __( 'Remove featured image', 'resource' ),
        'use_featured_image'    => __( 'Use as featured image', 'resource' ),
        'insert_into_item'      => __( 'Insert into Resource', 'resource' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Resource', 'resource' ),
        'items_list'            => __( 'Resources list', 'resource' ),
        'items_list_navigation' => __( 'Resources list navigation', 'resource' ),
        'filter_items_list'     => __( 'Filter Resources list', 'resource' ),
    );
    $args = array(
        'label'                 => __( 'Resource', 'resource' ),
        'description'           => __( 'Resources', 'resource' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'revisions', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-admin-page',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,       
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'resource', $args );

}
add_action( 'init', 'resource', 0 );