<?php

require(dirname(__FILE__).'/../../../../wp-load.php');

$recaptcha_response = $_POST['g-recaptcha-response'];
$to  = 'contact@hthsigns.com';
//$to  = 'me@joeshonm.com';
$name = $_POST['name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$message = $_POST['message'];

// message
$content = '
<!doctype html>
<html>
<head>
  <title>Hello HTH</title>
</head>
<body>
    <p>Hello HTH,</p>
    <p>From: '.$name.'</p>
    <p>Email: '.$email.'</p>
    <p>Telephone: '.(!empty($phone) ? $phone : "Not provided.").'</p>
    <p>Message:</p>
    <p>'.$message.'</p>
</body>
</html>
';

$headers  = "MIME-Version: 1.0" . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "From: ".$name." <".$email.">" . "\r\n";

//error_log($recaptcha_response);

$result = wp_mail( $to, 'Contact Form - Carsigns.com', $content, $headers );

wp_redirect( home_url('/contact-us?result='.$result) ); exit;