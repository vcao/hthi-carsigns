<?php

function etheme_get_option($key, $setting = null,$doshortcode = true) {
  	global $options;
    if ( function_exists( 'ot_get_option' ) ) {
    	if($doshortcode && is_string(ot_get_option( $key,$setting ))){
        	$result = do_shortcode(ot_get_option( $key,$setting ));
    	}else{
        	$result =  ot_get_option( $key,$setting );
    	}
    	return apply_filters('et_option_'.$key, $result);
    }
    
}
function etheme_option($key, $setting = null,$doshortcode = true) {
	echo etheme_get_option($key, $setting, $doshortcode);
}

/**
 * undocumented
 */
function et_is_blog () {
	global  $post;
	$posttype = get_post_type($post );
	return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
}
 
 
function etheme_get_custom_field($field, $postid = false) {
	global $post;
	if ( null === $post && !$postid) return FALSE;
	if(!$postid) {
		$postid = $post->ID;
	} 
	$page_for_posts = get_option( 'page_for_posts' );
	$custom_field = get_post_meta($postid, $field, true);
	
	if(is_array($custom_field)) {
		$custom_field = $custom_field[0];
	}
	if ( $custom_field ) {
		return stripslashes( wp_kses_decode_entities( $custom_field ) );
	}
	else {
		return FALSE;
	}
}
function etheme_custom_field($field) {
	echo etheme_get_custom_field($field);
}

function etheme_shortcode2id($shortcode, $type = 'page'){
	global $wpdb;
	$sql = "SELECT `ID` FROM `{$wpdb->posts}` WHERE `post_type` = '$type' AND `post_status` IN('publish','private') AND `post_content` LIKE '%$shortcode%' LIMIT 1";
	$page_id = $wpdb->get_var($sql);
	return apply_filters( 'etheme_shortcode2id', $page_id );
}

function etheme_tpl2id($tpl){
	global $wpdb;
	
	$pages = get_pages(array(
		'meta_key' => '_wp_page_template',
		'meta_value' => $tpl
	));
	foreach($pages as $page){
		return $page->ID;
	}
}

/**
 * undocumented
 */
function etheme_childtheme_file($file) {
	if ( ( PARENT_DIR != CHILD_DIR ) && file_exists(trailingslashit(CHILD_DIR).$file) ) 
		$url = trailingslashit(CHILD_URL).$file;
	else 
		$url = trailingslashit(PARENT_URL).$file;
	return $url;
}


// A callback function to add a custom field to our "product_cat" taxonomy
function product_cat_taxonomy_custom_fields($tag) {
   // Check for existing taxonomy meta for the term you're editing
    $t_id = $tag->term_id; // Get the ID of the term you're editing
    $term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check
?>

<tr class="form-field">
	<th scope="row" valign="top">
		<label for="hidden"><?php _e('Hide'); ?></label>
	</th>
	<td>
		<select class="postform" name="term_meta[hidden]" id="term_meta[hidden]">
			<option value="0" <?php echo !$term_meta['hidden'] ? 'selected' : ''; ?>>NO</option>
			<option value="1" <?php echo $term_meta['hidden'] ? 'selected' : ''; ?>>YES</option>
		</select>
		<br />
		<span class="description"><?php _e('Hide the Product Category'); ?></span>
	</td>
</tr>

<?php
}

// Add the fields to the "product_cat" taxonomy, using our callback function
add_action( 'product_cat_edit_form_fields', 'product_cat_taxonomy_custom_fields', 10, 2 );


// A callback function to save our extra taxonomy field(s)
function save_product_cat_custom_fields( $term_id ) {
    if ( isset( $_POST['term_meta'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_term_$t_id" );
        $cat_keys = array_keys( $_POST['term_meta'] );
            foreach ( $cat_keys as $key ){
            if ( isset( $_POST['term_meta'][$key] ) ){
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        //save the option array
        update_option( "taxonomy_term_$t_id", $term_meta );
    }
}

// Save the changes made on the "product_cat" taxonomy, using our callback function
add_action( 'edited_product_cat', 'save_product_cat_custom_fields', 10, 2 );
