<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}


// Magento Orders List
class Magento_Orders_List extends WP_List_Table {


    /** Class constructor */
    public function __construct() {

        

        parent::__construct( [
            'singular' => __( 'Magento Order', 'sp' ), //singular name of the listed records
            'plural'   => __( 'Magento Orders', 'sp' ), //plural name of the listed records
            'ajax'     => false //should this table support ajax?
        ] );

    }

    /**
    * Handles data query and filter, sorting, and pagination.
    */
    public function prepare_items() {

        global $wpdb;


        $per_page = 20;
        $current_page = $this->get_pagenum();
        $total_items = self::record_count();

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        

        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page                     //WE have to determine how many items to show on a page
        ));

        $this->items = $this->get_orders($per_page, $current_page);
    }

    public static function get_orders( $per_page = 5, $page_number = 1 ) {

        global $wpdb;

        $sql = "SELECT * FROM magento_sales_flat_order";

        if ( ! empty( $_REQUEST['orderby'] ) ) {
            $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
            $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
        }

        $sql .= " LIMIT $per_page";

        $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

        $result = $wpdb->get_results( $sql, 'ARRAY_A' );

        return $result;
    }

    function get_columns() {
        $columns = [
            'entity_id'    => __( 'Entity ID', 'sp' ),
            'customer_id' => __( 'Customer ID', 'sp' ),
            'customer_firstname'    => __( 'Customer First Name', 'sp' ),
            'customer_lastname'    => __( 'Customer Last Name', 'sp' )
        ];

        return $columns;
    }

    public static function record_count() {
        global $wpdb;

        $sql = "SELECT COUNT(*) FROM magento_sales_flat_order";

        return $wpdb->get_var( $sql );
    }

    public function no_items() {
        _e( 'No Magento Orders.', 'sp' );
    }

    function column_entity_id( $item ) {

        $title = '<strong>' . $item['entity_id'] . '</strong>';

        $actions = [
            'view' => sprintf( '<a href="?page=%s&action=%s&customer=%s">View Order</a>', esc_attr( $_REQUEST['page'] ), 'view', absint( $item['entity_id'] ) )
        ];

        return $title . $this->row_actions( $actions );
    }

    public function column_default( $item, $column_name ) {
        switch ( $column_name ) {
            case 'entity_id':
                return $item[ $column_name ];
            default:
                return $item[ $column_name ];
        }
    }

}

function magento_orders_index_page() {
    include get_template_directory().'/framework/magento-orders-templates/'.'index.php';
}

function magento_orders_menu() {
    add_menu_page( 'Magento Orders', 'Magento Orders', 'manage_options', 'magento-orders', 'magento_orders_index_page', 'dashicons-admin-page', 50  );
}

add_action( 'admin_menu', 'magento_orders_menu' );

